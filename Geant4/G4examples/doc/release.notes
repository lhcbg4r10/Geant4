!-----------------------------------------------------------------------------
! Package     : Geant4/G4examples
! Responsible : James McCarthy, Luca Pescatore
! Purpose     : Examples packages provided my Geant4, used as standalone tests
!-----------------------------------------------------------------------------

! 2016-04-27 - Marco Clemencic
 - Added dependency on ROOT for tests runtime.

! 2015-11-02 - Marco Clemencic
 - Added .gitignore file

!2015-07-29- Tim Williams
	- Installed scripts to run TestEm3 and TestEm5, can be run with 'Calorimeter_test' and 'MultipleScattering_test' respectively
	- Removed unused and unnecessary scripts from TestEm3 and TestEm5
!===================== Geant4/G4examples v6r1 2015-07-23 =====================
! 2015-07-23- Tim Williams
	- Updated scripts to run testEm3 and testEm5 to be compiler independent

! 2015-06-15- Luca Pescatore
	- Hadronic scripts and requirements modified to use python and cmt framework

!===================== Geant4/G4examples v6r0 2015-05-12 =====================
! 2015-05-12- Tim Williams
	-Added modified version of extended electromagnetic example TestEm5 to use as test of multiple scattering.  Used a copypatchsource.py script to copy private source and .hh files into place upoon build.

!===================== Geant4/G4examples v5r1 2015-05-11 =====================
! 2015-05-11- Tim Williams
	-Added copy patchedsource script and srcnew to TestEm3 to copy private source files into place upon build.

!===================== Geant4/G4examples v5r0 2015-03-06 =====================

! 2015-03-06 - Nigel Watson
 - Tagged for release with 9.6 series

! 2015-03-06 - Tim Williams
 - Updates for TestEm3 (new analysis included)

! 2014-07-24 - Gloria Corti
 - Modify requirements of subpackages to remove src and include directory
   otherwise made by cmt and copy is not done since directories exist

! 2014-07-16 - Gloria Corti
 - Remove unnecessary files from cmt directories, i.e. setup, version etc.

! 2014-07-16 - Gloria Corti
 - Modified all requirements files to use pattern linker_library instead of
   appending library to XXX_use_linkopts. This fixes compilation issues on
   slc5, see details on https://its.cern.ch/jira/browse/LHCBGAUSS-193

!===================== Geant4/G4examples v4r0 2014-07-11 =====================

! 2014-03-27 - Nigel Watson
 - Adapted requirements for nightly builds, following Marco Cl.'s
   recommendations.
!=============================================================================
