#!/usr/bin/env csh

#set echo on

set here = $PWD
set package = "G4hadlists"
set pack = "physics_lists"

set src_dir = ${G4SRC}/../${pack}/hadronic
set list = "LHEP/src QGSP_BERT_HP/src  LHEP_BERT_HP/src QGSP/src QGSP_HP/src Packaging/src"
set incl = "LHEP/include QGSP_BERT_HP/include LHEP_BERT_HP/include QGSP/include QGSP_HP/include Packaging/include"

cd $here/..

set n = 0
foreach p ($list[*])
    if !( -d $p ) then
      mkdir -p $p 
      cp -a ${src_dir}/${p}/*.* ${p}/.
    else
#     echo ' ' ${src_dir}/${p} ' exists - skip copy'
      @ n++
    endif
end
if ( $n <= 0 ) then 
  echo ' source files have been copied from '${src_dir}
else 
  echo ' source files exist - NO copy'
endif
#
# copy include files if not yet done
if !( -f ${package}.stamp) then 
  foreach p ($incl[*])
      cp -a ${src_dir}/${p}/*.* ${package}/.
  end
  touch ${package}.stamp
  echo ' include files have been copied from ' ${src_dir}
else 
  echo ' include files exist - NO copy'
endif

cd $here
unset echo


